import {ADD_TODO} from "./todosTypes";
let nextTodoId = 0;
export const addTodo = (payload) => {
    return {
        type: ADD_TODO,
        payload,
        id: nextTodoId++,
    }
};

