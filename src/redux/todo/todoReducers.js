import {ADD_TODO} from './todosTypes'

const initalState = {
  todos:[]
};

const todosReducers = (state=initalState,{type,payload,id}) => {
    switch (type) {
        case ADD_TODO:
            return {
                ...state,
                todos:[
                    ...state.todos,
                    {
                        id:id,
                        title:payload,
                        completed:false
                    }
                ]
            };
        default:
            return state
    }
}

export default todosReducers