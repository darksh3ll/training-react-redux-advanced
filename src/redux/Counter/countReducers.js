import { INCREMENT, DESINCREMENT } from './countTypes'

const initialState = {
  count: 0
};

const countReducers = (state = initialState, {type,payload}) => {
  switch (type) {
    case INCREMENT:
      return {
        ...state,
        count: state.count + payload
      };
    case DESINCREMENT:
      return {
        ...state,
        count: state.count - payload
      };
    default:
      return state
  }
};

export default countReducers
