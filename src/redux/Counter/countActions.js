import {INCREMENT,DESINCREMENT} from "./countTypes";


export const increment = (payload) => {
    return {
        type:INCREMENT,
        payload
    }
};

export const desincrement = (payload) => {
    return {
        type:DESINCREMENT,
        payload
    }
};