import React,{useState} from 'react'
import {useSelector,useDispatch} from "react-redux";
import {desincrement, increment} from "../redux/Counter/countActions";
import {addTodo} from "../redux/todo/todoActions";

const Home = () => {
  const count = useSelector(state => state.countReducers.count);
  const dispatch = useDispatch();

  const [todo,setTodo] = useState('');

    const _addTodos = () => {
        dispatch(addTodo(todo));
    };

  return (
      <div>
          <div className="container">
              <h1>Counter Redux</h1>
              <h1>{count}</h1>
              <button className="btn btn-dark" onClick={() => dispatch(increment(10))}>+</button>
              <button  className="btn btn-dark" disabled={count === 0} onClick={() => dispatch(desincrement(10))}>-</button>
          </div>

          <div className="container">
              <h1>Todos Redux</h1>
              <input onChange={(e) => setTodo(e.target.value)} type="text" placeholder="todo"/>
              <button onClick={_addTodos} className="btn btn-primary" >Add</button>
          </div>
      </div>


  )
};


export default Home
