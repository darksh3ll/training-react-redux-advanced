import React from 'react'
import { Route, Switch } from 'react-router-dom'
import Home from './Home'
import PrivaRoute from '../component/PrivateRoute'
import NotFound from './NotFound'

const App = () => {
  return (
    <Switch>
      <PrivaRoute exact path='/' component={Home} />
      <Route path='*' component={NotFound} />
    </Switch>
  )
}

export default App
